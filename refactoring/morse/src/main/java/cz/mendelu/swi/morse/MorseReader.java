package cz.mendelu.swi.morse;

import java.io.IOException;
import java.io.Reader;

public class MorseReader extends Reader {

	private final Reader in;

	private final MorseCode morseCode;

	private int lch;

	/**
	 * Veřejný konstruktor.
	 * @param in vsupem je textový soubor obsahující text v moresově abecedě odělený pomocí svislích lomítek.
	 */
	public MorseReader(Reader in) {
	this.in = in;
	this.morseCode = new MorseCode();
	try 
	{ this.lch = in.read(); 
	} catch (IOException e) 
	{ throw new IllegalStateException("Can't read first char", e);
	}
	}

	@Override
	public void close() throws IOException {
	in.close();
	}
	
	/**
	 * metoda na čtení
	 */
	@Override
	public int read(char[] cbuf, int off, int len) throws IOException {
	int l = -1; // nastavení příznaku pozice před první znak
		
	// Cyklus který iteruje přes všechny znaky z buferu
	for (int i = off; i < len; i++) { 
	int nch = -1;
	if (lch == -1) { nch = -1;
		} else {
			StringBuilder cb = new StringBuilder(); // Pomocá promněná do které se ukládají mezivýsledy.
			if (lch == '.' || lch == '-') {
			cb.append((char) lch);
			lch = -1;
					
			// Cyklus který načítá do znaky do pomocné proměné cb dokud nenajde znak |
			int nc2;
			while ((nc2 = in.read()) >= 0) {
			if (nc2 == '|') { // Podmínka tesující zda jsem jsem už načetl znak |
				lch = nc2; break;
			} else {
				// Nevím proč, ale musí se tu převádět pomocí metody sčítání řetězců.
				// NUTNO OPRAVIT !!!!
				cb.append((char) nc2); 
			}
			} nch = morseCode.decode(cb.toString());
					
// 			// Hledání dalšího znaku |
//			while ((nnc = in.read()) >= 0) {
//			if (nnc == '|') {
//			lch = nnc;
//			break;
//			} else {
//			cb.append((char) nnc);
//			}
//			}
	
			} else {
			lch = -1; // nastavení příznaku pozice před první znak

			// Druh sekvence:
			// 1. Písmeno
			// 2. mezera
			// 3. Konec věty
			int c = 1;
			int n2c;
			// Přesunuto z cyklu, ošetřena chyba kdy se v textu vyskytovalo více svislých lomítek za sebou
			// Opět cyklus na hledání znaku |
			while ((n2c = in.read()) >= 0) {
				if (n2c == '|') { c++; } else { lch = n2c; break; }}

			// Vypis podle druhu sekvence
			switch (c) {
				case 1:
					cb.append((char) lch); // buffer sekvence znaků
					lch = -1;
					int nnc;
					// Hledání dalšího znaku | (Zkrácená verze aby kód nebyl zbytečně dlouhý)
					while ((nnc = in.read()) >= 0) { if (nnc == '|') { lch = nnc; break; } else { 
						cb.append((char) nnc); // uložení znaku do buferu
					}}
					nch = morseCode.decode(cb.toString());
					break;
				case 2: // vypis mezery mezi slovy
					nch = ' '; 	break;
				case 3: // vypis tečky (konec věty)
					nch = '.'; break;
				default: // Oprava chyby kdy dekoder nepřekládal poslední písmeno
					int nc5;
					// Hledání dalšího znaku | (Zkrácená verze aby kód nebyl zbytečně dlouhý)
					while ((nc5 = in.read()) >= 0) { 
						if (nc5 == '|') { lch = nc5; break; } 
						else { cb.append((char) nc5); }
					}
					nch = morseCode.decode(cb.toString());
					}
				}

			}
	
			// Toto jsme převzal z přednášky, má to ukončit cyklus, když dojdou písmena.
			if (nch != -1) {
				cbuf[i] = (char) nch;
				l = (l == -1) ? 1 : l + 1;
			} else {
				break;
			}
		}
		return l;
	}

}
