package cz.mendelu.swi.morse;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.io.StringWriter;

import org.junit.Test;

public class MorseWriterTest {

	@Test
	public void testABC() throws IOException {
		StringWriter result = new StringWriter();
		MorseWriter writer = new MorseWriter(result);
		writer.write("abc");
		assertEquals(".-|-...|-.-.", result.getBuffer().toString());
	}

	@Test
	public void testTwoWords() throws IOException {
		StringWriter result = new StringWriter();
		MorseWriter writer = new MorseWriter(result);
		writer.write("a b");
		assertEquals(".-||-...", result.getBuffer().toString());
	}

	@Test
	public void testSentens() throws IOException {
		StringWriter result = new StringWriter();
		MorseWriter writer = new MorseWriter(result);
		writer.write("a.b");
		assertEquals(".-|||-...", result.getBuffer().toString());
	}

	@Test
	public void testWhiteCharCarReturn() throws IOException {
		StringWriter result = new StringWriter();
		MorseWriter writer = new MorseWriter(result);
		writer.write("a\rb");
		assertEquals(".-||-...", result.getBuffer().toString());
	}

}
