package jm.java.collections.exercise
/**
 * Created by xkoloma1 on 02.11.2016.
 */
class Ukol_5_Test extends GroovyTestCase {

    public void "test pridani studenta k registraci"() {
        Predmet predmet = new Predmet("PJJ");
        Registrace registrace = new Registrace(predmet)

        registrace.zaregistruj(new Student("Tomaš", "Novak"));
        registrace.zaregistruj(new Student("Jan", "Vopička"));
        registrace.zaregistruj(new Student("Jan", "Adamek"));

        Iterator<Student> studentIterator = registrace.getStudents().iterator();

        assert studentIterator.next().equals(new Student("Tomaš", "Novak"));
        assert studentIterator.next().equals(new Student("Jan", "Vopička"));
        assert studentIterator.next().equals(new Student("Jan", "Adamek"));
        assert studentIterator.hasNext() == false;
    }

    public void "test vícenásobné registrace jednoho studenta"() {
        Predmet predmet = new Predmet("PJJ");
        Registrace registrace = new Registrace(predmet)

        registrace.zaregistruj(new Student("Jan", "Adamek"));
        registrace.zaregistruj(new Student("Jan", "Adamek"));

        assert registrace.getStudents().size() == 1;
    }

    public void "test hromadného zapisu"() {
        Predmet predmet = new Predmet("PJJ");
        Registrace registrace = new Registrace(predmet)

        registrace.zaregistruj(new Student("Tomaš", "Novak"));
        registrace.zaregistruj(new Student("Jan", "Vopička"));
        registrace.zaregistruj(new Student("Jan", "Adamek"));

        predmet.zalozCviceni(2);
        predmet.zalozCviceni(2);

        assert registrace.hromadnyZapis() == 1
    }

    public void "test hromadného zapisu nad kapacitu"() {
        Predmet predmet = new Predmet("PJJ");
        Registrace registrace = new Registrace(predmet)

        registrace.zaregistruj(new Student("Tomaš", "Novak"));
        registrace.zaregistruj(new Student("Jan", "Vopička"));
        registrace.zaregistruj(new Student("Jan", "Adamek"));
        registrace.zaregistruj(new Student("Eva", "Marná"));
        registrace.zaregistruj(new Student("Petr", "Nový"));

        predmet.zalozCviceni(2);
        predmet.zalozCviceni(2);

        assert registrace.hromadnyZapis() == -1
    }

}
