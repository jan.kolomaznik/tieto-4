package jm.java.collections.exercise

/**
 * Created by xkoloma1 on 02.11.2016.
 */
class Ukol_1_Test extends GroovyTestCase {

    private static void prihlasXStudentuDoCviceni(Cviceni cviceni, int kolik) {
        for (int i = 0; i < kolik; i++) {
            Student s = new Student("jmeno_" + i, "prijmeni_" + i);
            assert cviceni.prednaska(s) == true;
        }
    }

    public void "test implementace metod 'hashCode()' a 'equals(Object o)'"() {
        Student a = new Student("Tomaš", "Novak");
        Student b = new Student("Jan", "Vopička");
        Student a2 = new Student("Tomaš", "Novak");

        assert a.equals(b) == false;
        assert a.equals(a2) == true;
    }

    public void "test prekroceni kapacity"() {
        Cviceni cviceni = new Cviceni(5);
        prihlasXStudentuDoCviceni(cviceni, 5);


        Student s = new Student("jmeno_X", "prijmeni_X");
        assert cviceni.prihlasit(s) == false;
        assert cviceni.getStudents().size() == 5;

    }

    public void "test opakovaneho pridani"() {
        Cviceni cviceni = new Cviceni(5);

        assert cviceni.prihlasit(new Student("jmeno", "prijmeni")) == true;
        assert cviceni.prihlasit(new Student("jmeno", "prijmeni")) == false;
        assert cviceni.getStudents().size() == 1;
    }

    public void "test zabezpecneho pridani"() {
        Cviceni cviceni = new Cviceni(5);
        prihlasXStudentuDoCviceni(cviceni, 5);

        shouldFail(RuntimeException) {
            cviceni.getStudents().add(new Student("Jack", "Sparrow"));
        }
    }
}
