package jm.java.collections.lession;

import static java.lang.Math.*;

import static jm.java.collections.lession.EnumPrava.Prava.*;


import java.util.EnumSet;

public class EnumPrava {

    public enum Prava {
        READ, WRITE, MONITOR, EXECUTE;
    }

    public static void granted(EnumSet<Prava> prava) {
        System.out.println(prava);
    }

    public static void main(String[] args) {
        granted(EnumSet.allOf(Prava.class));
        granted(EnumSet.of(WRITE));
        granted(EnumSet.range(READ, MONITOR));

    }
}
