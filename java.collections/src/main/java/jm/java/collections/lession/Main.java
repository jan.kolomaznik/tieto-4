package jm.java.collections.lession;

import java.util.*;

public class Main {

    private List<String> queue = new LinkedList<>();

    static enum DnyVTydnu {

        PO, UT, ST, CT, PA, SO, NE
    }

    public static void main(String... args) {
        EnumSet<DnyVTydnu> vikend = EnumSet.of(DnyVTydnu.SO, DnyVTydnu.NE);
        EnumSet<DnyVTydnu> pracovni = EnumSet.complementOf(vikend);

        System.out.println(vikend);
        System.out.println(pracovni);

        Set<DnyVTydnu> sVikend = Collections.synchronizedSet(vikend);
        Set<DnyVTydnu> lPracovni = Collections.unmodifiableSet(pracovni);

        lPracovni.remove(DnyVTydnu.PA);
        System.out.println(pracovni);


    }
}
