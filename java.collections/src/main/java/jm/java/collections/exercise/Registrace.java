package jm.java.collections.exercise;

import java.util.LinkedList;
import java.util.Queue;

public class Registrace {

    private Predmet predmet;

    private Queue<Student> studentQueue = new LinkedList<>();

    public Registrace(Predmet predmet) {
        this.predmet = predmet;
    }

    public void zaregistruj(Student student) {
        if (studentQueue.contains(student)) {
            return;
        }

        studentQueue.add(student);
    }

    public Queue<Student> getStudents() {
        return studentQueue;
    }

    public int hromadnyZapis() {
        int registrovano = studentQueue.size();
        int kapacita = predmet.getKapacitaPredmetu();

        for (int i = 0; i < Math.min(kapacita, registrovano); i++) {
            predmet.zapisDoPredmetu(studentQueue.poll());
        }

        return kapacita - registrovano;
    }
}
