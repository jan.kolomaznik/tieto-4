package jm.java.collections.exercise;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by xkoloma1 on 02.11.2016.
 */
public class Predmet {

    private final String kod;

    private List<Cviceni> cviceniList = new ArrayList<>();

    private SortedSet<Student> studentSortedSet = new TreeSet<>();

    public Predmet(String kod) {
        this.kod = kod;
    }

    public Cviceni zalozCviceni(int kapacita) {
        Cviceni cviceni = new Cviceni(kapacita, this);
        cviceniList.add(cviceni);
        return cviceni;
    }

    public Collection<Cviceni> getCvicenis() {
        return Collections.unmodifiableCollection(cviceniList);
    }

    public Cviceni getCviceni(int index) {
        return cviceniList.get(index);
    }

    public Collection<Student> getStudentyBezCviceni() {
        // TODO
        return Collections.EMPTY_SET;
    }

    public boolean zapisDoPredmetu(Student student) {
        return studentSortedSet.add(student);
    }

    public Collection<Student> getStudents() {
        return Collections.unmodifiableCollection(studentSortedSet);
    }

    @Override
    public String toString() {
        return "Predmet{" +
                "kod='" + kod + '\'' +
                ", cviceniList=" + cviceniList +
                ", studentSortedSet=" + studentSortedSet +
                '}';
    }

    public int getKapacitaPredmetu() {
        int result = 0;
        for (Cviceni cviceni: cviceniList) {
            result += cviceni.getKapacita();
        }
        return result;
    }

    private int kapacitaCviceni(Cviceni c) {
        return c.getKapacita();
    }
}
