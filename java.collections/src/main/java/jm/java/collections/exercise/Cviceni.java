package jm.java.collections.exercise;


import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by xkoloma1 on 02.11.2016.
 */
public class Cviceni {

    private final int kapacita;

    private Predmet predmet;

    private Set<Student> students = new HashSet<>();

    public Cviceni(int kapacita) {
        this(kapacita, null);
    }

    public Cviceni(int kapacita, Predmet predmet) {
        this.kapacita = kapacita;
        this.predmet = predmet;
    }

    public boolean prihlasit(Student student) {
        if (!jeMozneStudentaZpsatDoCviceni(student)) {
            return false;
        }

        if (kapacita <= students.size()) {
            return false;
        }

        return students.add(student);
    }

    private boolean jeMozneStudentaZpsatDoCviceni(Student student) {
        return predmet == null || predmet.getStudentyBezCviceni().contains(student);
    }

    public Collection<Student> getStudents() {
        return Collections.unmodifiableCollection(students);
    }

    public int getKapacita() {
        return kapacita;
    }

    @Override
    public String toString() {
        return "Cviceni{" +
                "kapacita=" + kapacita +
                ", students=" + students +
                '}';
    }
}
