package jm.java.collections.exercise;

import java.util.Objects;

/**
 * Created by xkoloma1 on 02.11.2016.
 */
public class Student implements Comparable<Student> {

    private final String jmeno;
    private final String prijmeni;

    public Student(String jmeno, String prijmeni) {
        this.jmeno = jmeno;
        this.prijmeni = prijmeni;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return Objects.equals(jmeno, student.jmeno) &&
                Objects.equals(prijmeni, student.prijmeni);
    }

    @Override
    public int hashCode() {
        return Objects.hash(jmeno, prijmeni);
    }

    @Override
    public int compareTo(Student o) {
        if (!Objects.equals(this.prijmeni, o.prijmeni)) {
            return this.prijmeni.compareTo(o.prijmeni);
        }
        if (!Objects.equals(this.jmeno, o.jmeno)) {
            return this.jmeno.compareTo(o.jmeno);
        }
        return 0;
    }

    @Override
    public String toString() {
        return "Student{" +
                "jmeno='" + jmeno + '\'' +
                ", prijmeni='" + prijmeni + '\'' +
                '}';
    }
}
