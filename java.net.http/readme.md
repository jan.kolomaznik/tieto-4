[remark]:<class>(center, middle)
# Síťové operace
[remark]:<slide>(new)
## HTTP klient
* Pro připojení prostřednictvím protokolu HTTP slouží třída `HttpURLConnection` (jejím potomkem je velice podobná třída `HttpsURLConnection` pro šifrované připojení přes SSL).

* `HttpURLConnection` je abstraktní třída, její instance tedy nemůžeme vytvářet.

Instanci získáme např. metodou `openConnection()` (jako parametr konstruktoru zadáme instanci třídy URL) – tato metoda však vrátí instanci `URLConnection`, proto si ji musíme přetypovat.

[remark]:<slide>(new)
### HTTP klient: příklad
```java
try {
   URL url = new URL("http://www.pef.mendelu.cz/");    
   HttpURLConnection con = (HttpURLConnection) url.openConnection();
   
   System.out.println("Response code: " + con.getResponseCode());
   System.out.println("Content type: " + con.getContentType());
   
   BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
   
   String s = br.readLine();
   while (s != null) {
       System.out.println(s);
    s = br.readLine();
   }
   
   con.disconnect();
} catch (Exception e) {
   ...
}
```

#### Notes
Příklad ukazuje jednoduché získání dokumentu z HTTP serveru. Na základě URL je vytvořeno spojení na server, příslušná instance třídy HttpURLConnection sama odešle požadavek. Příklad vypíše kód odpovědi serveru a MIME typ dat. Pak se získá vstupní stream a data z něj se vypisují na standardní výstup.