package cz.ictpro.java.design.patterns;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Arrays;

public class Observer {

    // B
    public static class Person {

        private String name;

        private int age;

        private final PropertyChangeSupport pcs;

        public Person(String name, int age) {
            this.name = name;
            this.age = age;
            this.pcs = new PropertyChangeSupport(this);
        }

        public void addPropertyChangeListener(PropertyChangeListener listener) {
            pcs.addPropertyChangeListener(listener);
        }

        public void removePropertyChangeListener(PropertyChangeListener listener) {
            pcs.removePropertyChangeListener(listener);
        }

        public String getName() {
            return name;
        }

        public void setName(String newName) {
            String oldName = name;
            this.name = newName;
            pcs.firePropertyChange("name", oldName, newName);
        }

        public int getAge() {
            return age;
        }

        public void setAge(int newAge) {
            int oldAge = age;
            this.age = newAge;
            pcs.firePropertyChange("age", oldAge, newAge);
        }

        @Override
        public String toString() {
            return "Person{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }
    }

    // A
    public static class PersonManager {

        private Person[] person;

        public PersonManager(Person ... person) {
            this.person = person;
            Arrays.stream(person)
                    .forEach(p -> p.addPropertyChangeListener(this::personMonitor));
        }


        public void personMonitor(PropertyChangeEvent evt) {
            System.out.println(evt);
        }
    }

    public static void main(String[] args) {
        Person honza = new Person("Honza", 87);
        Person jirka = new Person("Jirka", 75);

        PersonManager personManager = new PersonManager(honza, jirka);

        honza.setAge(30);
        jirka.setName("Vera");

    }
}
