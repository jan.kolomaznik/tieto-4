[remark]:<class>(center, middle)
# Spring Framework
## Dependency injection

[remark]:<slide>(new)
## Inversion of Control
týká se komponentních systémů s možností řídit životní cyklus komponenty

operační prostředí (většinou aplikační rámec, aplikační server, kontejner, middleware) se stará o hlavní kostru řídící tok výpočtu: která komponenta bude instanciována, co se na ní kdy zavolá za metodu, jak bude vyřízen klientský požadavek...

také označováno jako "The Hollywood Principle": Do not call us, we will call you!

většinou jde o nejpodstatnější rys aplikačních rámců, často ve spojení s MVC

velmi často v podobě speciálního případu IoC: Dependency Injection

[remark]:<slide>(new)
### Pevným vazbám se snažíme vyhýbat!
```java
public interface CarDao {}

public class CarDaoImpl implements CarDao {}

// zde mezi CarDao a CarService je pevná vazba
public class CarServiceImpl {
        private CarDao carDao = new CarDaoImpl();
}
```

[remark]:<slide>(wait)
**Proč?** 
- Vaše architektura je příliš úzce svázaná (podobně jako u dědičnosti) a málo flexibilní. 
- Změna kódu může být velice obtížná. 
- Třída bez pevných vazeb se lépe testuje.

[remark]:<slide>(wait)
**Jak?** 
- Princip IoC přesouvá odpovědnost za vznik vazeb na někoho jiného. 
- V našem případě je přesunut z programátora na framework.

[remark]:<slide>(new)
## Dependency injection (DI)
Tento návrhový vzor souvisí přímo s IoC. 

Jedná se o mechanismus, kdy je do naší třídy vložena (injektována) instance jiné třídy. 

O tuto injekci se stará sám Framework podle konfigurace. 

Pro přehlednost a větší flexibilitu je dobré mít oddělenu konfiguraci od implementace. 

Existují tři typy injekce:

[remark]:<slide>(wait)
### Property inject
Framework si sám najde danou property pomocí reflexe.

```java
@Autowired
private CarDao carDao;
```

[remark]:<slide>(new)
### Constructor inject
Při vytváření pošle přes konstruktor instance potřebných component.

```java
private CarDao carDao;

@Autowired
public CarServiceImpl(CarDao carDao) {
        this.carDao = carDao;
}
```

[remark]:<slide>(wait)
### Setter inject
Při vytváření je nahrána instance pomocí setter-u.

```java
private CarDao carDao;

@Autowired
public void setCarDao(CarDao carDao) {
        this.carDao = carDao;
}
```
