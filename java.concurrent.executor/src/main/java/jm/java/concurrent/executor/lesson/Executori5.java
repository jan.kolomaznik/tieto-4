package jm.java.concurrent.executor.lesson;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

public class Executori5 {


    static Callable<String> callable(String result, long sleepSeconds) {
        return () -> {
            System.out.printf("Start: %s%n", result);
            try {
                TimeUnit.SECONDS.sleep(sleepSeconds);
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.out.printf("Done: %s%n", result);
            return result;
        };
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException, TimeoutException {
        ExecutorService executor = Executors.newCachedThreadPool();
        TimeUnit.SECONDS.sleep(10);
        List<Callable<String>> callables = Arrays.asList(
                callable("task1", 20),
                callable("task2", 10),
                callable("task3", 30));

        String result = executor.invokeAny(callables);
        System.out.println(result);
    }
}
