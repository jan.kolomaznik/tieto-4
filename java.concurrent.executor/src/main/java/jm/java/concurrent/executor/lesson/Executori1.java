package jm.java.concurrent.executor.lesson;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Executori1 {

    public static void main(String[] args) {
        ExecutorService executor = Executors.newWorkStealingPool();
        for (int i = 0; i < 100; i++) {
            executor.submit(() -> {
                String threadName = Thread.currentThread().getName();
                System.out.println("Hello " + threadName);
            });
        }
        System.out.println("Done");

    }
}
