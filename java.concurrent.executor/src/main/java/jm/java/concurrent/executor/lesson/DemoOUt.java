package jm.java.concurrent.executor.lesson;

import java.io.*;
import java.util.Arrays;
import java.util.List;

public class DemoOUt {

    public static class Person implements Serializable {

        private String name;
        private List<String> nicknames;
        private String surname;
        private int age;
        private double weight;

        @Override
        public String toString() {
            return "Person{" +
                    "name='" + name + '\'' +
                    ", nicknames=" + nicknames +
                    ", surname='" + surname + '\'' +
                    ", age=" + age +
                    ", weight=" + weight +
                    '}';
        }
    }

    public static void main(String[] args) {
        Person p = new Person();
        p.age = 10;
        p.name = "Honza";
        p.nicknames = Arrays.asList("AAA", "BBB", "CCC");
        p.surname = null;
        p.weight = 15978.5489;

        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("person"))) {

            oos.writeObject(p);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
