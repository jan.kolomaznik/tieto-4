package jm.java.concurrent.executor.lesson;

import java.util.concurrent.*;

public class Executori3 {

    public static void main(String[] args) throws ExecutionException, InterruptedException, TimeoutException {
        ExecutorService executor = Executors.newFixedThreadPool(1);
        Future<Integer> future = executor.submit(() -> {
            try {
                TimeUnit.SECONDS.sleep(2);
                System.out.println("Task done.");
                return 123;
            }
            catch (InterruptedException e) {
                e.printStackTrace();
                throw new IllegalStateException("task interrupted", e);
            }
        });
        System.out.println(future.isDone());
        try {
            System.out.println(future.get(1, TimeUnit.SECONDS));
        } catch (TimeoutException e) {
            e.printStackTrace();
            future.cancel(false);
        }
        System.out.println(future.isCancelled());
    }
}
