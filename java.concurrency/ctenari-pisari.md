[remark]:<class>(center, middle)

Paralelní programování
======================
Synchronizace – čtenáři – písaři
--------------------------

[remark]:<slide>(new)
## Čtenáři – písaři

* více konzumentů
* více producentů
* konzument neodebírá data z paměti
  * čtení se může provádět souběžně
  * zápis vylučuje čtení a naopak
    * ale od každého může být více instancí
  * kategorické vyloučení

[remark]:<slide>(new)
## Zámek
* Sofistikovanější verze synchonizovaného bloku.
* Interface `Lock` a jeho základní implementace jsou umístěny v balíčku `java.util.concurrent.locks`.
* Verze se synchonizační blokem:

```java
public int inc(){
    synchronized(this){
        return ++count;
    }
}
```

[remark]:<slide>(new)
* Verze se zámkem:

```java
public int inc(){
    lock.lock();
    int newCount = ++count;
    lock.unlock();
    return newCount;
}
```

[remark]:<slide>(new)
### Výhody zámku oproti *synchronized*
* Synchronizovaný blok neposkytuje žádné záruky pořadí, ve kterém bude čekajícím na vstup udělen přístup.
* Nelze nastavit žádné parametry vstupu do synchronizovaného bloku.
    * Například časový limit
    * Test zda je to možné
* Synchronizované blok musí být plně obsaženy v rámci jedné metody.

[remark]:<slide>(new)
### Základní metody:
* `lock()`
* `lockInterruptibly()`
* `tryLock()`
* `tryLock(long timeout, TimeUnit timeUnit)`
* `unlock()`

[remark]:<slide>(wait)
#### Instance
* [ReentrantLock](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/locks/ReentrantLock.html): Základní
* [StampedLock](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/locks/StampedLock.html): Rozšířený například o metody optimistického přístupu.

[remark]:<slide>(new)
### ReadWriteLock
* Speciální zámek umožňující souběžné čtení, ale výlučný zápis

```java
ReadWriteLock readWriteLock = new ReentrantReadWriteLock();
```

[remark]:<slide>(wait)
* Skládá se ze dvou zámků:
    * `readLock()`
    * `writeLock()`

[remark]:<slide>(new)
### Práce se zámakem pro čtení
```java
readWriteLock.readLock().lock();
    // multiple readers can enter this section
    // if not locked for writing, and not
    // writers waiting to lock for writing.
readWriteLock.readLock().unlock();
```

[remark]:<slide>(wait)
### Práce se zámakem pro zápis
```java
readWriteLock.writeLock().lock();
    // only one writer can enter this section,
    // and only if no threads are currently reading.
readWriteLock.writeLock().unlock();
```
