[remark]: <> (class: center, middle)

Paralelní programování
======================
-------------------------

## Vytváření procesů
* Procesy reprezentuje třída `java.lang.Process`.
* Vytvořit ji lze dvěma způsoby:

```java
Runtime.getRuntime().exec(command);
```

```java
ProcessBuilder pb = new ProcessBuilder(
         "myCommand", "myArg1", "myArg2");
Map<String, String> env = pb.environment();
env.put("VAR1", "myValue");
env.remove("OTHERVAR");
pb.directory(new File("myDir"));
File log = new File("log");
pb.redirectErrorStream(true);
pb.redirectOutput(Redirect.appendTo(log));
Process p = pb.start();
p.waitFor();
```

---
## Komunikace s procesy
* Probíhá pomocí prostředků poskytovaných operačním systém
* Nejsnadnější způsob je pomocí `stdin`/`stdout`

**Zaslání zprávy procesu `p`**

```java
OutputStream stdin = p.getOutputStream();
stdin.write(message);
stdin.flush();
```

**Čtení zprávy od procesu `p`**

```java
InputStream stdout = p.getInputStream();
message = stdin.read();
```

---
## Vláknově (ne)bezpečné proměnné
* Lokální proměnná jsou vždy.

```java
public void someMethod(){
  long threadSafeInt = 0;
  threadSafeInt++;
}
```

---
* Reference na lokální proměnné pouze pokud neopustí metodu.

```java
public void someMethod(){
  LocalObject localObject = new LocalObject();
  localObject.callMethod();
  method2(localObject);
}

public void method2(LocalObject localObject){
  localObject.setValue("value");
}
```

---
## Vláknově nebezpečné proměnné
* Proměnné instance nebo proměnné třídy *nejsou vláknově bezpečné*.

```java
NotThreadSafe sharedInstance = new NotThreadSafe();
new Thread(new MyRunnable(sharedInstance)).start();
new Thread(new MyRunnable(sharedInstance)).start();
public class MyRunnable implements Runnable {
  NotThreadSafe instance = null;
  public MyRunnable(NotThreadSafe instance){
    this.instance = instance;
  }
  public void run(){
    this.instance.add("some text");
  }
}
```

---
### Immutable objekty jsou Vláknově bezpečné
* Proto se je snažte používat co nejčastěji :)

```java
public class ImmutableValue{
  private int value = 0;
  public ImmutableValue(int value){
    this.value = value;
  }
  public int getValue(){
    return this.value;
  }
  public ImmutableValue add(int valueToAdd){
      return new ImmutableValue(this.value + valueToAdd);
  }
}
```

---
class: middle
* Ale POZOR! Přestože immutable jsou vláknově bezpečné, s jejich referencemi to již neplatí.

```java
public class Calculator{
  private ImmutableValue currentValue = null;
  public ImmutableValue getValue(){
    return currentValue;
  }
  public void setValue(ImmutableValue newValue){
    this.currentValue = newValue;
  }
  public void add(int newValue){
    this.currentValue = this.currentValue.add(newValue);
  }
}
```

* _Určíte v čem je problém?_

---
## Práce Javy s pamětí - logický pohled
* Pro porozumění práce více vláknových aplikací je velmi důležité rozumět tomu, jak JVM pracuje s pamětí.
* Java rozděluje paměť na zásobníky vláken a heap

![Rozdělení paměti](media/java-memory-model-1.png)

---
class: middle
* Na zásobníku jsou uloženy všechny lokální proměnné a reference na objekty.

---
### Uspořádání v dat v paměti - logický pohled
* Platí že:
    * Primitivní datové typu jsou uložená na zásobníku.
    * Objekty jsou uloženy na heap.

![Uspořádání v paměti](media/java-memory-model-3.png)

---
## Hardware mapování logického pohledu
* Jak se dá očekávat, je poněkud odlišné o logické architektury, velmi zjednodušená vypadá nějak takto ...

![Hardware model paměti](media/java-memory-model-4.png)

---
## Mapování mezi modely
* Logický a fyzický model je nutné namapovat na sebe navzájem.

![Mapování modelů](media/java-memory-model-5.png)

---
class: middle
* Dva hlavní problémy:
    * Viditelnost mezi vlákny změněných proměnných
    * Kontrola přístupu při čtení a zapisování sdílených proměnných

---
### Viditelnost sdílených proměnných
* problém s proměnnými uloženými v lokálních Caches bez řádné synchronizace

![Sdílené promněnné](media/java-memory-model-6.png)

---
class: middle
* Klíčové slovo `volatile` zabrání uložení proměnné do CPU Cache.

```java
public volatile int counter = 0;
```

---
### Proměnné sdílené přes více vláken
* klíčové slovo `volatile` použijeme pokud potřebujeme zajistit viditelnost změn proměnné mezi různými vlákny.

![Volatile](media/java-volatile-1.png)

---
### Synchronizace proměnných
* Druhým problémem je synchronizace práce s proměnnou při souběžném update.

![Sdílené promněnné](media/java-memory-model-7.png)

---
class: middle
* Tento problém se řeší prostřednictvím klíčového slova `synchronized`.

```java
  public synchronized void add(int value){
      this.count += value;
  }
```

---
## Java Synchronized Blocks
* Klíčové synchronize můžeme použít:
    1. U metody instance
    2. U statické metody
    3. Jako blok v kódu metody instance
    4. Jako blok v kódu statické metody

---
### Add 1. Synchronizovaná metoda

```java
  public synchronized void add(int value){
      this.count += value;
  }
```

* Vlastní objekt je použit synchronizažní monitor.
* Do tako označených může vstoupit současně jen jedno vlákno.
* _Tedy jedno vlákno do jednoho objektu_

---
### Add 2. Synchronizovaná statická metoda

```java
  public static synchronized void add(int value){
      this.count += value;
  }
```

* Velmi podobné jako u předchozího, ale jako zámek je použit *class* objekt třídy.
* Platí, že v celé JVM existuje vždy právě jeden objekt pro každou třídu.

---
### Add 3. Synchronizovaný blok v metodě instance

```java
  public void add(int value){
    synchronized(this){
       this.count += value;
    }
  }
```

* Do bloku kódu chráněného monitorem *this* může současně vstoupit jen jedno vlákno.
*_Je/Není toto shodné s bodem 1?_
