package jm.java.streams.exercise;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Exercise: resolve the tasks in the code below.
 */
public final class CollectionBasics {

    /**
     * Runs the program.
     *
     * @param args
     *            command line arguments. It must not be {@code null}.
     */
    public static void main(String... args) {
        Random random = new Random();
        //final List<Integer> numbers = random.ints(100)
        //        .mapToObj(i -> Integer.valueOf(i))
        //        //.mapToObj(Integer::valueOf)
        //        .collect(Collectors.toList()); // TODO: Make a list of random numbers

        final List<Integer> numbers = Stream.generate(random::nextInt)
                .limit(10)
                .collect(Collectors.toList()); // TODO: Make a list of random numbers

        numbers.stream().filter(i -> i % 2 == 0).forEach(System.out::println);
        // TODO: Remove all even numbers

        // TODO: Sort the list
        // TODO: Print the remaining numbers, one on each line
        // TODO: Do it with lambda and with new methods on the collection
        //System.out.println(numbers);
        // TODO: Rewrite all above with a single stream and no intermediate collection
    }
}
