package jm.java.streams.lesson;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class FlatMap {

    public static void main(String[] args) {
        Consumer<String> print = System.out::println;

        String veta = "Přiliš žluťoučký kůň úpěl ďábelské ódy.";
        String[] slova = veta.split("\\s");

        final Map<String, Integer> counter = new HashMap<>();
        long count = Arrays.stream(slova)
                .parallel()
                .peek(print)
                .flatMap(s -> Arrays.stream(s.split("")))
                .peek(print)
                .count();
        System.out.println(count);
    }
}
