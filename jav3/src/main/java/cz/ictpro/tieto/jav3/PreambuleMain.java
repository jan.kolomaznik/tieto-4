package cz.ictpro.tieto.jav3;

import sun.reflect.annotation.AnnotatedTypeFactory;

import java.lang.annotation.Annotation;
import java.util.Arrays;

public class PreambuleMain {

    public static void main(String[] args) {
        MyDemoClass myDemo = new MyDemoClass();
        Class demoClass = myDemo.getClass();

        Arrays.stream(demoClass.getAnnotations())
                .forEach(a -> System.out.println(a));

        Annotation annotation = demoClass.getAnnotation(ClassPreamble.class);
        ClassPreamble classPreamble = (ClassPreamble) annotation;
        System.out.println(classPreamble.author());

    }
}
