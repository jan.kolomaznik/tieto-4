package cz.ictpro.tieto.jav3;

import java.lang.reflect.*;
import java.util.Objects;

public class ReflectionDemo {

    public int add(int a, int b) {
        return a + b;
    }

    public static void main(String args[]) {
        int a = 0;
        while (true) {
            a += 1;
            int r = reflection(a);
            int c = clasic(a);
            if (r != c) {
                throw new IllegalArgumentException();
            }
        }
    }

    static Class cls;
    static Method meth;

    static {
        try {
            cls = Class.forName("cz.ictpro.tieto.jav3.ReflectionDemo");
            Class partypes[] = {Integer.TYPE, Integer.TYPE};
            meth = cls.getMethod("add", partypes);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    private static int reflection(int a) {
        try {
            Object methobj = cls.newInstance();
            Object arglist[] = {new Integer(a), new Integer(a)};
            Object retobj = meth.invoke(methobj, arglist);
            return ((Integer)retobj).intValue();
        } catch (Throwable e) {
            throw new UnsupportedOperationException(e);
        }
    }

    private static int clasic(int a) {
        ReflectionDemo reflectionDemo = new ReflectionDemo();
        return reflectionDemo.add(a, a);
    }
}
