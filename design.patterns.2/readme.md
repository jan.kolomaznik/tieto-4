[remark]:<class>(center, middle)
# Přehlednávrhových vzorů:  
----
*Řízení počtu instancí*

[remark]:<slide>(new)
## Library Class
Tento návrhový vzor použijeme v případě, že nechceme vytvářet žádný objekt, ale pouze zapouzdřit množinu funkcí/metod.

Protože nepotřebujeme instanci, je dobré zakázat její vytvoření.

V anglické terminologii se pro tento vzor používá termín *Utility*, známe například třídu `StringUtility`

[remark]:<slide>(wait)
```java
public class PointUtility {
    
*   private PointUtility() {}
    
    public static double distance(Point a, Point b) {
        ...
    }
    
    public static Point multiple(Point p, double d) {
        ...
    }
}
```

[remark]:<slide>(new)
## Singleton
Asi nejznámější návrhový vzor, popisuje způsob, jak vytvořit nejvýše jednu instanci.

Existují dva případy - s brzkou/pozdní inicializaci.

*Poznámka: třída vracejí jedináčka nemusí vracet vlastní instanci.*

[remark]:<slide>(wait)
```java
public class Singleton {
    
    private static volatile Singleton instance = null;
    
    public static Singleton getInstance() {
        if (instance == null) {
*           synchronized (Singleton.class) {
                if (instance == null) {
                    instance = new Singleton();
                }
            }
        }
        return instance;
    }
    
*   private Singleton() {}
    
}
```

[remark]:<slide>(new)
## Enumerated Type
Výčtový typ slouží k vytvoření předem známého počtu instancí.

V Javě je od verze 5 přímo součástí jazyky pomocí `enum`.

[remark]:<slide>(wait)
```java
public enum Fruit {
    
    APPLE, PEAR 
    
}
```

[remark]:<slide>(new)
## Original
Tento návrhový vzor použijeme v případě, kdy víme že v aplikaci budeme použít malí počet **různých** instancí třídy, ale různých místech kódu.

Často rovněž platí, že objekty implementují návrhový vzor **Imutable** a tedy je možné je bezpečně používat opakovaně.

[remark]:<slide>(wait)
```java
public class Color {
    private static Map<String, Color> instanceMap = new HashMap<>();
    
    public static final Color RED =   color("#FF0000");
    public static final Color GREEN = color("#00FF00");
    public static final Color BLUE =  color("#0000FF");
    
    public Color color(String code) {
        if (instanceMap.containsKey(code))
            return instanceMap.get(code);
        Color color = new Color(code);
        instanceMap.put(code, color);
        return color;
    } 
    
    private final String code;
    
    private Color(String code) {
        ...
    }
} 
```
[remark]:<slide>(new)
## Pool
Tento návrhový vzor umožňuje omezit počet existujících instancí.

V případě potřeby nové instance se používá již nepotřebná instance.

Na rozdíl od předchozího návrhového vzoru `Originál` není potřeba, aby instance byly neměnné.

Java podporuje thread pooling prostřednictvím třídy `java.util.concurrent.ExecutorService`.

[remark]:<slide>(wait)
![](media/Object_pool.svg)

*Příklad: [PooledObject](snippets/cz/ictpro/lectures/java/patterns/PooledObject.java) a
                  
[remark]:<slide>(new)
## Simple pool
```java
public abstract class ObjectPool<T extends Resetable> {

  private Set<T> available = new HashSet<>();
  private Set<T> inUse = new HashSet<>();

  protected abstract T create();

  public synchronized T getObject() {
    if (available.isEmpty()) {
      available.add(create());
    }
    T instance = available.iterator().next();
    available.remove(instance);
    inUse.add(instance);
    return instance;
  }

  public synchronized void releaseObject(T instance) {
    instance.reset();  
    inUse.remove(instance);
    available.add(instance);
  }
}
```

[remark]:<slide>(new)
## Flyweight
Tento vzor používáme ve chvíli, kdy řešíme problém velkého množství instancí. Často uváděným případem jsou písmena v knize.

Řešením je rozdělení objektů do skupin podle charakteristik:
- pro každou skupinu tvoříme jednoho reprezentanta,
- ten uchovával shodné charakteristiky,
- rozdílné se předávají jako parametry metod.

![](media/Flyweight.svg)

[remark]:<slide>(new)
### Demo example

```java
public interface Shape {
   void draw();
}
```
[remark]:<slide>(wait)
Create concrete class implementing the same interface.

```java
public class Circle implements Shape {
   private String color;
   private int x;
   private int y;
   private int radius;

   public Circle(String color){ this.color = color; }

   public void setX(int x) { this.x = x; }

   public void setY(int y) { this.y = y; }

   public void setRadius(int radius) { this.radius = radius; }

   @Override
   public void draw() {
      System.out.println("Circle: Draw() " +
        "Color: " + color + ", x: " + x + ", y:" + y + ", radius:" + radius);
   }
}
```

[remark]:<slide>(new)
Create a factory to generate object of concrete class based on given information.

```java
public class ShapeFactory {

   // Uncomment the compiler directive line and
   // javac *.java will compile properly.
   // @SuppressWarnings("unchecked")
   private static final HashMap circleMap = new HashMap();

   public static Shape getCircle(String color) {
      Circle circle = (Circle)circleMap.get(color);

      if(circle == null) {
         circle = new Circle(color);
         circleMap.put(color, circle);
         System.out.println("Creating circle of color : " + color);
      }
      return circle;
   }
}
```

[remark]:<slide>(new)
Use the factory to get object of concrete class by passing an information such as color.

```java
public class FlyweightPatternDemo {
   private static final String colors[] = 
                            { "Red", "Green", "Blue", "White", "Black" };
   
   public static void main(String[] args) {
      for(int i=0; i < 20; ++i) {
         Circle circle = (Circle)ShapeFactory.getCircle(getRandomColor());
         circle.setX(getRandomX());
         circle.setY(getRandomY());
         circle.setRadius(100);
         circle.draw();
      }
   }
   
   private static String getRandomColor() {
      return colors[(int)(Math.random()*colors.length)];
   }
   
   private static int getRandomX() {
      return (int)(Math.random()*100 );
   }
   
   private static int getRandomY() {
      return (int)(Math.random()*100);
   }
}
```
