[remark]:<class>(center, middle)
# Java enum

[remark]:<slide>(new)
## Implementace enum pomocí tříd


[remark]:<slide>(new)
#### Ukázka použití třídy EnumMap:
```java
import java.util.EnumMap;

enum Obdobi {
   JARO, LÉTO, PODZIM, ZIMA;
}

public class EnumMapPokus {

   public static void main(String[] args) {
      EnumMap em = new EnumMap(Obdobi.class);
      em.put(Obdobi.JARO, "Otepluje se");
      em.put(Obdobi.PODZIM, "Ochlazuje se");
                
   }
}
```

[remark]:<slide>(new)
### Kolekce EnumSet
* Kolekce `EnumSet` představuje množinu, jejíž prvky jsou konstanty výčtového typu.

* Metody třídy EnumSet:
  - `allOf(Class e)` – vytvoří množinu obsahující všechny konstanty výčtového typu určeného class-objektem e.
  - `noneOf(Class e)` – Vytvoří prázdnou množinu hodnot výčtového typu určeného classobjektem e.
  - `of(E p, E... o)` – Vytvoří množinu obsahující zadané konstanty výčtového typu E.
  - `range(E od, E do)` –Vytvoří množinu obsahující všechny konstanty výčtového typu E ze zadaného rozsahu.

[remark]:<slide>(new)
#### Příklad práce s EnumSet
Pokud budeme mít např. výčtový typ `public enum DnyVTydnu {PO, UT, ST, CT, PA, SO, NE}`, můžeme vyzkoušet provést následující metody:

```java
import java.util.EnumSet;

public class Mnozina {
   public static void main(String[] args) {
      EnumSet vsechny = EnumSet.allOf(DnyVTydnu.class);
      EnumSet nektere = EnumSet.of(DnyVTydnu.PA, DnyVTydnu.SO, DnyVTydnu.NE);
      EnumSet rozsah = EnumSet.range(DnyVTydnu.PO, DnyVTydnu.PA);
      System.out.println(vsechny);
      System.out.println(nektere);
      System.out.println(rozsah);
   }
}
```