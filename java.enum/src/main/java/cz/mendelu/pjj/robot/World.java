package cz.mendelu.pjj.robot;

/**
 * Created by Honza on 08.11.2016.
 */
public class World {

    private final int width;
    private final int height;
    private Object[][] map;

    public World(int width, int height) {
        this.width = width;
        this.height = height;
        this.map = new Object[width][height];
    }

    public Object getTreasureAt(int x, int y){
        // FIXME Upravit v samostatné práci
        if (x < 0) {
            return null;
        } else if (x >= width) {
            return null;
        } else if (y < 0) {
            return null;
        } else if (y >= height) {
            return null;
        } else {
            return map[x][y];
        }
    }

    public void removeTreasureAt(int x, int y){
        // FIXME Upravit v samostatné práci
        if (x >= 0 && x < width && y >= 0 && y < height) {
            map[x][y] = null;
        }
    }

    public Object addTreasureAt(Object object, int x, int y){
        map[x][y] = object;
        return object;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

}
