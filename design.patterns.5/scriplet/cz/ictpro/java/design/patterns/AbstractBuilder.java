package cz.ictpro.java.design.patterns;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import java.util.ArrayList;
import java.util.List;

public class AbstractBuilder {

    public static void main(String[] args) {
        HouseBuilder builder = new XMLHouseBuilder(20);
        System.out.println(builder
                .base('#')
                .addLevel()
                .roof(5)
                .toHouse()
        );
    }


    public interface HouseBuilder {

        HouseBuilder base(char material);

        HouseBuilder roof(int height);

        HouseBuilder addLevel();

        String toHouse();
    }

    public static class XMLHouseBuilder implements HouseBuilder {

        private Document document;
        private Element root;

        public XMLHouseBuilder(int size) {
            document = DocumentHelper.createDocument();
            root = DocumentHelper.createElement("root");
            document.setRootElement(root);
            root.addAttribute("size", Integer.toString(size));
        }

        public HouseBuilder base(char material) {
            root.addElement("base")
                .addAttribute("material", Character.toString(material));
            return this;
        }

        public HouseBuilder roof(int height) {
            root.addElement("roof")
                .addAttribute("height", Integer.toString(height));
            return this;
        }

        public HouseBuilder addLevel() {
            root.addElement("level");
            return this;
        }

        public String toHouse() {
            return document.asXML();
        }
    }

    public static class AsciiHouseBuilder implements HouseBuilder {

        public static class House {

            private Base base;
            private Roof roof;
            private List<Level> levels;

            public static class Base {

                private final char material;

                private final int size;

                public Base(char material, int size) {
                    this.material = material;
                    this.size = size;
                }

                @Override
                public String toString() {
                    StringBuilder builder = new StringBuilder();
                    for (int i = 0; i < size; i++) {
                        builder.append(material);
                    }
                    builder.append('\n');
                    return builder.toString();
                }
            }

            public static class Level {

                private int size;

                public Level(int size) {
                    this.size = size;
                }

                @Override
                public String toString() {
                    StringBuilder builder = new StringBuilder();
                    for (int i = 0; i < 4; i++) {
                        draw(builder, ' ', 2);
                        draw(builder, '|');
                        draw(builder, (i == 0) ? '-': ' ', size - 6);
                        draw(builder, '|');
                        draw(builder, ' ', 2);
                        newLine(builder);
                    }
                    return builder.toString();
                }
            }

            public static class Roof {

                private final int height;
                private final int size;

                public Roof(int size, int height) {
                    this.height = height;
                    this.size = size;
                }

                @Override
                public String toString() {
                    StringBuilder builder = new StringBuilder();
                    for (int i = height - 1; i >= 0; i--) {
                        draw(builder, ' ', i);
                        draw(builder, '/');
                        draw(builder, '~', size - (2 * i) - 2);
                        draw(builder, '\\');
                        draw(builder, ' ', i);
                        newLine(builder);
                    }
                    return builder.toString();
                }
            }

            public House(Base base, Roof roof, List<Level> levels) {
                this.base = base;
                this.roof = roof;
                this.levels = levels;
            }

            @Override
            public String toString() {
                StringBuilder builder = new StringBuilder();
                if (roof != null) {
                    builder.append(roof.toString());
                }
                for (Level level: levels) {
                    builder.append(level.toString());
                }
                if (base != null) {
                    builder.append(base.toString());
                }
                return builder.toString();
            }
        }

        private static void draw(StringBuilder builder, char c) {
            builder.append(c);
        }

        private static void draw(StringBuilder builder, char c, int length) {
            for (int i = 0; i < length; i++) {
                builder.append(c);
            }
        }

        private static void newLine(StringBuilder builder) {
            builder.append('\n');
        }

        private int size;

        private House.Base base;
        private House.Roof roof;
        private List<House.Level> levels = new ArrayList<>();

        public AsciiHouseBuilder(int size) {
            this.size = size;
        }

        public AsciiHouseBuilder base(char material) {
            base = new House.Base(material, size);
            return this;
        }

        public AsciiHouseBuilder roof(int height) {
            roof = new House.Roof(size, height);
            return this;
        }

        public AsciiHouseBuilder addLevel() {
            levels.add(new House.Level(size));
            return this;
        }

        public String toHouse() {
            return new House(base, roof, levels).toString();
        }
    }

}
