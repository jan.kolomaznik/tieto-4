package jm.java.threads.lesson;

import static java.lang.System.currentTimeMillis;

public class Demo {

    public static void killPenguin(long time) {
        long start = currentTimeMillis();
        while (currentTimeMillis() - start < time) {
        }
    }

    private boolean done = false;

    public void foo() {
                killPenguin(10000);
                System.out.println("foo");
                while (!done);
                killPenguin(10000);
                System.out.println("foo aster boo");

    }

    public  void boo() {

            killPenguin(10000);
            System.out.println("boo");
            done = true;
            killPenguin(10000);
    }

    public static void main(String[] args) throws InterruptedException {
        killPenguin(1000);
        Demo demo = new Demo();
        Thread t1 = new Thread(demo::foo);
        t1.setDaemon(false);
        t1.start();
        Thread t2 = new Thread(demo::foo);
        t2.setDaemon(true);
        t2.start();

    }
}
