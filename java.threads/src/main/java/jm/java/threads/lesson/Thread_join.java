package jm.java.threads.lesson;

public class Thread_join {

    static class ProduceThread extends Thread {

        public ProduceThread() {
            super("ProduceThread");
        }

        @Override
        public void run() {
            try {
                for (int i = 1; i <= 3; i++) {
                    System.out.printf("%s: %d.%n", getName(), i);
                    Thread.sleep(1000);
                }
            } catch (InterruptedException e) {
                System.out.printf("Jsem vzhuru - %s.%n", getName());
            }
        }
    }

    static class JoinedThread extends Thread {

        private Thread thread;

        public JoinedThread(Thread thread) {
            super("JoinedThread");
            this.thread = thread;
        }

        @Override
        public void run() {
            System.out.println("Start");
            try {
                thread.start();
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Stop");
        }
    }

    public static void main(String[] args) throws InterruptedException {
        ProduceThread pt = new ProduceThread();
        JoinedThread jp = new JoinedThread(pt);
        jp.start();
        Thread.sleep(1500);
        pt.interrupt();
    }
}