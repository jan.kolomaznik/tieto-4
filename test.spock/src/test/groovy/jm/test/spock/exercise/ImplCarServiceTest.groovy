package jm.test.spock.exercise

import spock.lang.Specification
import spock.lang.Unroll

public class ImplCarServiceTest extends Specification {

    private ImplCarService carService;

    def setup() {
        carService = new ImplCarService();
    }

    public void CreateCar() {
        given: ""
        CarRepository carRepository = Mock(CarRepository);
        carService.carRepository = carRepository

        when:
        Car car = carService.createCar("Beriska");

        then:
        2 * carRepository.save(_) >> new Optional<>()
        assert car != null

    }

    def "Plus test: #a + #b = #c"() {
        expect:
        carService.plus(a, b) == c

        where:
        a | b | c
        1 | 2 | 3
        3 | 6 | 10

    }

    def "CreateId"() {
    }
}
