package jm.test.spock.exercise;

import java.util.Optional;

//@Service
public class ImplCarService implements CarService {

    //@Autowird
    private CarRepository carRepository;

    //@Autowird
    private RentService rentService;

    @Override
    public Car createCar(String name) {
        Car car = new Car();
        car.setName(name);
        Optional<Car> save = carRepository.save(car);
        return save.orElse(null);
    }

    @Override
    public Car rentCar(Car car, String user) {
        if (rentService.canRent(car, user)) {
            car.setRent(true);
            return carRepository.save(car).get();
        } else {
            throw new UnsupportedOperationException("Car cant by rent.");
        }
    }

    @Override
    public int createId(String name) {
        return name.hashCode();
    }

    private int plus(int a, int b) {
        return a + b;
    }
}
