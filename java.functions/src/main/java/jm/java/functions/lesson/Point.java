package jm.java.functions.lesson;

import static java.lang.Math.abs;

public interface Point {

    double getX();

    double getY();

    default double distance(Point point) {
        return abs(getX() - point.getX()) + abs(getY() - point.getY());
    }
}
