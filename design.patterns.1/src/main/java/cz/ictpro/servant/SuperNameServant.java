package cz.ictpro.servant;

public class SuperNameServant {

    interface SuperNameServanted {

        String getName();
    }

    public String doSuperName(SuperNameServanted servanted) {
        return servanted.getName().toUpperCase();
    }
}
