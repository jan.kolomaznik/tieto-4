package cz.ictpro.servant;

public class Main {

    private static final Main main2 = init();
    private static final Main main = main2;

    public static Main init() {
        return new Main();
    }

    public static void main(String[] args) {
        Obsluhovana1 obsluhovana1 = new Obsluhovana1();
        Obsluhovana2 obsluhovana2 = new Obsluhovana2();
        Obsluhovana2 NULL = new Obsluhovana2() {
            @Override
            public String getSuperName() {
                return super.getSuperName();
            }

            @Override
            public String getName() {
                return super.getName();
            }
        };

        DnyVTydnuEnum.PO.getOznaceni();

        System.out.println(obsluhovana1.getSuperName());
        System.out.println(obsluhovana2.getSuperName());
    }

    private Main() {
        System.out.println(main);
    }
}
