package cz.ictpro.servant;

public class Obsluhovana1 implements SuperNameServant.SuperNameServanted {

    private String name = Obsluhovana1.class.getSimpleName();

    private SuperNameServant servant = new SuperNameServant();

    public String getSuperName() {
        return servant.doSuperName(this);
    }

    @Override
    public String getName() {
        return name;
    }
}
