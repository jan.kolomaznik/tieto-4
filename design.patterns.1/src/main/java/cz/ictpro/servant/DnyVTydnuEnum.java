package cz.ictpro.servant;

public enum DnyVTydnuEnum {

    PO("PO") {
        @Override
        public boolean isWeekend() {
            return false;
        }
    },
    UT("UT") {
        @Override
        public boolean isWeekend() {
            return true;
        }
    };

    private final String oznaceni;

    DnyVTydnuEnum(String oznaceni) {
        this.oznaceni = oznaceni;
    }

    public String getOznaceni() {
        return oznaceni;
    }

    public abstract boolean isWeekend();
}
