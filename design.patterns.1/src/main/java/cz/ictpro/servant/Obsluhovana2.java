package cz.ictpro.servant;

public class Obsluhovana2 implements SuperNameServant.SuperNameServanted {

    private String balicek = Obsluhovana2.class.getPackage().getName();

    private SuperNameServant servant = new SuperNameServant();

    public String getSuperName() {
        return servant.doSuperName(this);
    }

    @Override
    public String getName() {
        return balicek;
    }
}
