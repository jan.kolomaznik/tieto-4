package cz.ictpro.servant;

public abstract class DnyVTydnu {

    public static final DnyVTydnu PO = new DnyVTydnu("PO") {
        @Override
        public boolean isWeekend() {
            return false;
        }
    };
    public static final DnyVTydnu UT = new DnyVTydnu("UT") {
        @Override
        public boolean isWeekend() {
            return false;
        }
    };
    public static final DnyVTydnu ST = new DnyVTydnu("ST") {
        @Override
        public boolean isWeekend() {
            return false;
        }
    };
    public static final DnyVTydnu CT = new DnyVTydnu("CT") {
        @Override
        public boolean isWeekend() {
            return false;
        }
    };
    public static final DnyVTydnu PA = new DnyVTydnu("PA") {
        @Override
        public boolean isWeekend() {
            return false;
        }
    };
    public static final DnyVTydnu SO = new DnyVTydnu("SO") {
        @Override
        public boolean isWeekend() {
            return true;
        }
    };
    public static final DnyVTydnu NE = new DnyVTydnu("NE") {
        @Override
        public boolean isWeekend() {
            return true;
        }
    };

    private final String oznaceni;

    private DnyVTydnu(String oznaceni) {
        this.oznaceni = oznaceni;
    }

    public String getOznaceni() {
        return oznaceni;
    }

    public abstract boolean isWeekend();
}
