[remark]:<class>(center, middle)
# Anotace

[remark]:<slide>(new)
## Co jsou to anotace
Anotace jsou formou metadat, poskytují informace o programu, ale které nejsou součástí samotného programu. 

Anotace nemají přímý vliv na fungování kódu, který anotují.

Anotace mají řadu využití, mezi nimi:

- Informace pro překladač
- Zpracování času kompilace a času implementace
- Běhové zpracování

[remark]:<slide>(new)
### Formát anotace
Znak znaku (`@`) označuje překladateli, že následující je anotace. 

Ve své nejjednodušší podobě mohou anotace vypadat takto:

```java
@Entity
```

[remark]:<slide>(wait)
Anotace může obsahovat prvky, které mohou být pojmenovány:

```java
@Autor(
   jméno = "Benjamin Franklin",
   datum = "3/27/2003"
)
class MyClass () {...}
```

[remark]:<slide>(wait)
nebo pokud je pouze jeden prvek s názvem `value`, pak může být název vynechán::

```java

@SuppressWarnings ("unchecked")
void myMethod () {...}
```

[remark]:<slide>(new)
### Vlastnosti anotace
Pokud má anotace žádné prvky, mohou být vynechány závorky, jak je ukázáno v předchozím příkladu `@Entity`.

Na stejné prohlášení je také možné použít více poznámek:

```java
@Author (jméno = "Jane Doe")
@Ebook
class MyClass {...}
```

[remark]:<slide>(wait)
### Opakující se anotace
Pokud mají anotace stejný typ, pak se toto nazývá opakující se anotace:

```java
@Author (jméno = "Jane Doe")
@Author (jméno = "John Smith")
class MyClass {...}
```

[remark]:<slide>(new)
### Kde lze anotace použít
Anotace mohou být použity pro deklarace: **deklarace tříd**, **polí**, **metod**. 

Od vydání verze Java SE 8 lze anotace použít i pro definici **typů**:

[remark]:<slide>(wait)
#### Výraz vytvoření instance instance:
```java
new @Interned MyObject ();
```

[remark]:<slide>(wait)
#### Přetypování:
```java
myString = (@NonNull String) str;
```

[remark]:<slide>(wait)
#### U generyckých typů:
```java
class UnmodifiableList <T> implementuje
    @Readonly List <@Readonly T> {...}
```

[remark]:<slide>(wait)
#### U výjimek
```java
void monitorTemperature() throws @Critical TemperatureException { ... }
```

[remark]:<slide>(new)
## Předdefinované typy anotaci
Seznam některých annotaci z Java API.

[remark]:<slide>(wait)
### Annotace používané v kódu

#### @Deprecated
`@Deprecated` označuje prvek, který je zastaralý a již by se neměl používat. 

Kompilátor generuje varování vždy, když program používá metodu, třídu nebo pole s anotací @Deprecated. 

Pokud je prvek zastaralý, měl by být také zdokumentován pomocí tagu Javadoc @deprecated, jak je ukázáno v následujícím příkladu. 

```java
/**
 * @Deprecated
 * vysvětlení, proč bylo zastaralé
 */
@Deprecated
static void deprecatedMethod() {
    ...
}
```

[remark]:<slide>(new)
#### @Override 
@Override informuje překladač, že metoda je překrytá.

```java
@Override
int overriddenMethod() {
    ...
}
```

Tato annotace není povinná, ale pomáhá předejít chyám.

Pokud takto označená metoda správě nepřekrývá, kompilátor generuje chybu.


[remark]:<slide>(wait)
#### @SuppressWarnings 
@SuppressWarnings potlačuje specifická upozornění, která by jinak vygenerovala. 

V následujícím příkladu je použita zastaralá metoda a kompilátor obvykle generuje varování. 

V tomto případě však anotace způsobí potlačení výstrahy.

```java
@SuppressWarnings ("deprecation")
void useDeprecatedMethod() {
    // deprecation warning
    // - suppressed
    objectOne.deprecatedMethod();
}
```

Každé varování kompilátoru patří do kategorie, pokud chceme potlačit více varování, použijeme pole.
```java
@SuppressWarnings({"unchecked", "deprecation"})
```

[remark]:<slide>(new)
#### @SafeVarargs 
@SafeVarargs anotace, při použití metody nebo konstruktoru, tvrdí, že kód neprovádí potenciálně nebezpečné operace na jeho parametru varargs. P

okud je použit tento typ anotace, jsou zrušeny nezaškrtnuté varování týkající se používání varargs.

[remark]:<slide>(wait)
#### @FunctionalInterface 
@FunctionalInterface, zavedená v jazyce Java SE 8.

Označuje, že deklarace iinterface má být funkčním rozhraním.

[remark]:<slide>(new)
### Anotace pro anotace
Java definuje i anotace pro annotace (meta-anotace)

Existuje několik typů meta-anotací definovaných v atributu java.lang.notation.

[remark]:<slide>(wait)
#### @Retention 
@Retention annotation určuje způsob prace s anotací:

- `RetentionPolicy.SOURCE` - Označená anotace zůstane zachována pouze na zdrojové úrovni a kompilátor ji ignoruje.
- `RetentionPolicy.CLASS` - Označená anotace zůstává kompilátorem v době kompilace, ale Java Virtual Machine (JVM) ji ignoruje.
- `RetentionPolicy.RUNTIME` - Označená poznámka je zachována JVM, takže může být použita v runtime prostředí.

[remark]:<slide>(wait)
#### @Documented 
@Documented annotace označuje, že kdykoli se použije specifikovaná anotace, mělo by být její použítí zaneseno do javadoc.

Ve výchozím nastavení nejsou anotace v programu Javadoc zahrnuty.

[remark]:<slide>(new)
#### @Target 
@Target annotation označuje, pto jaký typ prvků jazyka lze anotaci použít. 

Cílová poznámka určuje jeden z následujících typů prvků jako jejich hodnotu:

- `ElementType.ANNOTATION_TYPE` - lze použít pro typ anotace.
- `ElementType.CONSTRUCTOR` - může být použit na konstruktoru.
- `ElementType.FIELD` - lze použít na pole nebo vlastnost.
- `ElementType.LOCAL_VARIABLE` - lze použít na místní proměnnou.
- `ElementType.METHOD` - lze použít na anotaci na úrovni metody.
- `ElementType.PACKAGE` - lze použít na deklaraci balíčku.
- `ElementType.PARAMETER` - lze aplikovat na parametry metody.
- `ElementType.TYPE` - lze aplikovat na libovolný prvek třídy.

[remark]:<slide>(wait)
#### @Inherited 
@Inherited anotace označuje, že typ anotace může být zděděna z předka. 

Ve výchozím nastavení se anotace nedědí.

[remark]:<slide>(wait)
#### @Repeatable 
@Repeatable označuje, že anotace může být použita více než jednou.
 
[remark]:<slide>(new)
## Vytváření anotace
Například potřebujete u každé třídu v kódu pamatovat určitá metadata:

```java
public class Generation3List extends Generation2List {

   // Author: John Doe
   // Date: 3/17/2002
   // Current revision: 6
   // Last modified: 4/12/2004
   // By: Jane Doe
   // Reviewers: Alice, Bill, Cindy

   // class code goes here

}
```

[remark]:<slide>(new)
### Definice anotace
Chcete-li přidat toto stejné metadata s anotací, musíte nejprve definovat typ anotace. 

Syntaxe pro to je:

```java
@interface ClassPreamble {
   String author();
   String date();
   int currentRevision() default 1;
   String lastModified() default "N/A";
   String lastModifiedBy() default "N/A";
   // Note use of array
   String[] reviewers();
}
```

Tělo anotace obsahuje deklarace prvků anotace, které vypadají spíš jako metody. 

Všimněte si, že mohou definovat volitelné výchozí hodnoty.

[remark]:<slide>(new)
### Použití anotace
Použití naší annotace:

```java
@ClassPreamble (
   author = "John Doe",
   date = "3/17/2002",
   currentRevision = 6,
   lastModified = "4/12/2004",
   lastModifiedBy = "Jane Doe",
   // Note array notation
   reviewers = {"Alice", "Bob", "Cindy"}
)
public class Generation3List extends Generation2List {

// class code goes here

}
```

[remark]:<slide>(new)
### Dokumentace anotace
Chcete-li, aby se informace v `@ClassPreamble` objevily v dokumentaci generované službou Javadoc, musíte dp definice `@ClassPreamble` přidat annotaci `@Documented`:

```java
// import this to use @Documented
import java.lang.annotation.*;

@Documented
@interface ClassPreamble {

   // Annotation element definitions
   
}
```

[remark]:<slide>(new)
## JSR 308: Java Type Annotations 
Od Javy veze 8 lze použi Checker Framework

Více onformací v článku [JSR 308 Explained](https://www.oracle.com/technetwork/articles/java/ma14-architect-annotations-2177655.html)

O [Checker Framework](https://checkerframework.org/)

[Live demo](http://eisop.uwaterloo.ca/live/#mode=edit)
