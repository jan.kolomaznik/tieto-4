[remark]:<class>(center, middle)
# Java Invokedynamic
## JVM 101  

[remark]:<slide>(new)
## Uvod

Tato novinka je zavedena od Java 7.
 
Pro vývojáře jazyka Java nejsou natolik důležité, mají ale zásadní dopad na:
- Dyamické jazyku v JVM (Groovy, JPython, JRuby, Scala ...)
- Klíčocé pro lambda funkcí v Java 8.

V rámci této přednášky se ponoříme hlouběji do *invokedynamic* a ukážem si, proč se jedná o tak klíčový nástroj.

rvní práce na invokedynamicu přichází nejméně do roku 2007.
 
*invokedynamic* je pozoruhodné v tom, že to byl první nový bytecode od doby Java 1.0. 

Připojil se k existujícím příkazům bytekodes:
- *invokevirtual* - standardní volání metod
- *invokestatic* - volání statických metod
- *invokeinterface* - volání metody přes rozhraní
- *invokespecial* - používá se, když je vyžadováno "přesné" odeslání (`super`)

[remark]:<slide>(new)
#### Příklady volání těchto opcodes:

```java
public class InvokeExamples {
    public static void main(String[] args) {
        InvokeExamples sc = new InvokeExamples();
        sc.run();
    }

    private void run() {
        List<String> ls = new ArrayList<>();
        ls.add("Good Day");

        ArrayList<String> als = new ArrayList<>();
        als.add("Dobrý den");
    }
}
```

[remark]:<slide>(new)
#### ByteCode

Tím vzniká bytecode, že se můžeme rozebrat pomocí nástroje javap:

```
avap -c InvokeExamples.class

public class demo.InvokeExamples {
  public demo.InvokeExamples();
    Code:
       0: aload_0
       1: invokespecial #1                  // Method java/lang/Object."<init>":()V
       4: return

  public static void main(java.lang.String[]);
    Code:
       0: new           #2                  // class demo/InvokeExamples
       3: dup
       4: invokespecial #3                  // Method "<init>":()V
       7: astore_1
       8: aload_1
       9: invokespecial #4                  // Method run:()V
      12: return
```
[remark]:<slide>(new)
```
  private void run();
    Code:
       0: new           #5                  // class java/util/ArrayList
       3: dup
       4: invokespecial #6                  // Method java/util/ArrayList."<init>":()V
       7: astore_1
       8: aload_1
       9: ldc           #7                  // String Good Day
      11: invokeinterface #8,  2            // InterfaceMethod java/util/List.add:(Ljava/lang/Object;)Z
      16: pop
      17: new           #5                  // class java/util/ArrayList
      20: dup
      21: invokespecial #6                  // Method java/util/ArrayList."<init>":()V
      24: astore_2
      25: aload_2
      26: ldc           #9                  // String Dydh Da
      28: invokevirtual #10                 // Method java/util/ArrayList.add:(Ljava/lang/Object;)Z
      31: pop
      32: return
}
```

Toto příklad znázorňuje tři ze čtyř použití invoke.

Za prvé, můžeme vidět, že obě volání (v bajtech 11 a 28 v metodě run):
- `ls.add("Good Day")`
- `als.add("Dobrý den")`

V Java kódu vypadají podobně, ale v bytekoce jsou odlišné.
 
[remark]:<slide>(new)
### `invokeinterface` 

Pro javac má proměnná ls statický typ List<String>a seznam je rozhraní. 

Adresy metod rozhraní jsou umístění v tabulce "vtable".
 
Adresa metoda `add()` nebyla stanovena v době kompilace. 

Pro vykonání je tedy nejprve nutné vyhledat adresu metody `add()`.

Příkaz `invokeinterface` a odkládá skutečné vyhledávání metody až do doby běhu
- kdy může být zkontrolován skutečný stav v vtable
- může byt určena adresa metody `add()`.

[remark]:<slide>(new)
### `invokevirtual` 

Volání als.add("Dobrý den") přijímá als a statický typ třídy - ArrayList<String>. 

To znamená, že umístění metody vtable je známo v době kompilace. 

Javac je proto schopen vydat invokevirtual instrukci pro přesnou vtable položku. 

Konečná volba metody, která je stále určena při běhu, protože to umožňuje možnost přepsání metody, ale vtable slot byl stanoven v době kompilace.

[remark]:<slide>(new)
### `invokespecial`  
Příklad také ukazuje dva možné případy využití invokespecial. 

Tato opcode se používá kdy volání je jasné z přdkladu.
 
Kdy není možné překrývat metody. 

Dva případy, které tento příklad demonstruje, jsou soukromé metody a super <init> , protože konstruktory p5edka jsou známy v době kompilace a nemohou být potlačeny.

[remark]:<slide>(wait)
### Otázky
Všechny volání metod Javě jsou implementovány jednoho z těchto čtyř možností.

- Proč tedy `invokedynamic`?
- Proč je to pro vývojáře Java užitečné?

[remark]:<slide>(new)
### Co přináeší `invokedynamic`

Hlavním cílem je vytvořit bytecode pro vykonání nového typu volání metod.

To by mělo umožnit na aplikační urovni určil, jaký způsob bude volání bude použit.

To umožňuje vytvářeta podporovat mnohem dynamičtější javyky a programovací styly než dříve.
 
Uživatelský kód může za běhu rozhodovat o volání metod, aniž by došlo k problémy s výkonností a bezpečnostními problémů souvisejícímí s používáním reflexe. 

Jedním z cílu je, aby volání `invokedynamic` bylo stejně rychlé jako volání klasické metody (`invokevirtual`).

S Java 7, JVM měla podporu nové bytecode instrukce, ale java kompilátor nikdy nevytvořil bytecode, který by tuto novou instrukci použil. 

Místo toho byla tato funkce použita k podpoře dynamických jazyků běžících na JVM.

Od Javy 8, se `invokedynamic` a používána k implementaci výrazů lambda a defualt metod.

Pro vývojáře aplikací Java však stále není žádná přímá cesta k plnému dynamickému řešení. 

Jiný způsob pv Jazyce Java neexistuje.

To znamená, že mechanismus zůstává nejasný pro většinu vývojářů Java, navzdory síle, kterou poskytuje. 

[remark]:<slide>(new)
## Úvod do úchytů metod
Aby invokedynamic fungoval správně, je klíčovým pojmem metodika. Toto je způsob, jak představovat metodu, která by měla být volána z invokedynamického místa volání. Obecně platí, že každá invokedynamická instrukce je spojena se speciální metodou (známou jako metoda bootstrap nebo BSM). Když je interpretorem dosaženo invokedynamické instrukce, volá se BSM. Vrátí objekt (obsahující popisovač metod), který označuje, jakou metodu má být volání skutečně spuštěno.

To je poněkud podobné odrazu, ale reflexe má omezení, která z něj činí nevhodnou pro použití s ​​invokedynamic. Namísto toho byly java.lang.invoke.MethodHandle (a podtřídy) přidány do API jazyka Java 7, aby představovaly metody, na které se může cílit invokedy. Třída MethodHandle obdrží od JVM speciální ošetření, aby správně fungovala.

Jedním ze způsobů, jak přemýšlet o metodách úchytů, je reflexe jádra provedená bezpečným a moderním způsobem s maximálním možným typem bezpečnosti. Jsou potřebné pro invokedynamic, ale mohou být také použity samostatně.

Typy metod
Metoda Java může být považována za složenou ze čtyř základních částí:

název
Podpis (včetně typu návratu)
Třída, kde je definována
Bytecode, která implementuje danou metodu
To znamená, že pokud chceme odkazovat na metody, potřebujeme způsob, jak účinně reprezentovat signatury metod (spíše než používat hrozné třídy <?> [] Hacks, které je odraz nucen používat).

Jinak řečeno, jeden z prvních stavebních prvků potřebných pro manipulace s metodami je způsob, jak reprezentovat metodické podpisy pro vyhledávání. V metodě Method Handles API, zavedené v jazyce Java 7, je tato role splněna třídou java.lang.invoke.MethodType, která používá nezastupitelné instance reprezentující podpisy. Metoda MethodType získáte metodou methodType (). Jedná se o metodu variadic, která vezme objekty třídy jako argumenty.

Prvním argumentem je objekt třídy odpovídající návratu typu podpisu; zbývající argumenty jsou objekty třídy, které odpovídají typům argumentů metody v podpisu. Například:

```java
// Signature of toString()
MethodType mtToString = MethodType.methodType(String.class);

// Signature of a setter method
MethodType mtSetter = MethodType.methodType(void.class, Object.class);

// Signature of compare() from Comparator<String>
MethodType mtStringComparator = MethodType.methodType(int.class, String.class, String.class);
```

Pomocí metody MethodType ji můžeme nyní použít společně s názvem a třídou, která definuje metodu vyhledávání způsobu zpracování. Chcete-li to provést, musíme zavolat statickou metodu MethodHandles.lookup (). To nám dává "vyhledávací kontext", který je založen na přístupových právech aktuálně prováděné metody (tj. Metody nazvané vyhledávání ()).

Objekt kontextu vyhledávání obsahuje řadu metod, které mají názvy začínající na "find", např. FindVirtual (), findConstructor (), findStatic (). Tyto metody vrátí skutečný popisovač metody, ale pouze v případě, že kontext vyhledávání byl vytvořen metodou, která by mohla přistupovat (volat) požadovanou metodu. Na rozdíl od reflexe neexistuje způsob, jak tuto kontrolu přístupu narušit. Jinými slovy metoda handleů nemá žádný ekvivalent metody setAccessible (). Například:

```java
public MethodHandle getToStringMH() {
    MethodHandle mh = null;
    MethodType mt = MethodType.methodType(String.class);
    MethodHandles.Lookup lk = MethodHandles.lookup();

    try {
        mh = lk.findVirtual(getClass(), "toString", mt);
    } catch (NoSuchMethodException | IllegalAccessException mhx) {
        throw (AssertionError)new AssertionError().initCause(mhx);
    }

    return mh;
}
```

Na metodě MethodHandle existují dvě metody, které lze použít k vyvolání metody handle invoke () a + invokeExact (). Obě metody berou argumenty přijímače a volání jako parametry, takže podpisy jsou:

```java
public final Object invoke(Object... args) throws Throwable;
public final Object invokeExact(Object... args) throws Throwable;
```

Rozdíl mezi těmito dvěma je, že invokeExact () se pokusí zavolat metodu přímo pomocí přesných argumentů. Na druhou stranu, invoke () má schopnost mírně změnit argumenty metody v případě potřeby. invoke () provede konverzi asType (), která může konvertovat argumenty podle této sady pravidel:

Pokud je to nutné, budou k dispozici primitivní boxy
Boxované primitivy budou v případě potřeby unboxed
V případě potřeby se rozšiřují
Typ neplatného návratu bude převeden na hodnotu 0 (pro primitivní návratové typy) nebo null pro typy návratů, které očekávají referenční typ
Hodnoty null se považují za správné a procházejí bez ohledu na statický typ
Podívejme se na jednoduchý příklad vyvolání, který bere v úvahu tato pravidla:

```java
Object rcvr = "a";
try {
    MethodType mt = MethodType.methodType(int.class);
    MethodHandles.Lookup l = MethodHandles.lookup();
    MethodHandle mh = l.findVirtual(rcvr.getClass(), "hashCode", mt);

    int ret;
    try {
        ret = (int)mh.invoke(rcvr);
        System.out.println(ret);
    } catch (Throwable t) {
        t.printStackTrace();
    }
} catch (IllegalArgumentException | NoSuchMethodException | SecurityException e) {
    e.printStackTrace();
} catch (IllegalAccessException x) {
    x.printStackTrace();
}
```

V složitějších příkladech mohou metodické úchyty poskytnout mnohem jasnější způsob, jak provádět stejné dynamické programovací úlohy jako reflexe jádra. Nejen to, ale způsoby manipulace byly od počátku navrženy tak, aby lépe fungovaly s modelem nízké úrovně výkonu JVM a potenciálně mohou poskytovat mnohem lepší výkon (ačkoli výkonový příběh se stále rozvíjí).

Metoda Rukojeti a invokedynamic
Metoda invokedynamic uses handles pomocí mechanismu metody bootstrap. Na rozdíl od invokevirtual, invokedynamic instrukce nemají žádný objekt přijímače. Místo toho se chovají jako invokestatické a používají BSM k vrácení objektu typu CallSite. Tento objekt obsahuje popisovač metod (nazvaný "cíl"), který představuje metodu, která má být provedena jako výsledek invokedynamické instrukce.

Když je zařazena třída obsahující invokedynamic, hovory se říká, že jsou ve stavu "unlaced" a po návratu BSM se výsledná CallSite a metodika popisuje jako "přivázaná" do místa volání.

Podpis pro BSM vypadá takto (všimněte si, že BSM může mít nějaké jméno):

```java
static CallSite bootstrap(MethodHandles.Lookup caller, String name, MethodType type);
```

Pokud chceme vytvořit kód, který skutečně obsahuje invokedynamic, budeme muset použít knihovnu manipulace bytecode (jelikož jazyk Java neobsahuje konstrukce, které potřebujeme). Ve zbytku tohoto článku budeme muset použít knihovnu ASM pro generování bytecode, která obsahuje invokedynamické pokyny. Z hlediska aplikací Java se tyto objevují jako soubory s normálními třídami (ačkoli samozřejmě nemají žádné možné reprezentace zdrojového kódu jazyka Java). Jsou považováni za kód Java jako "černé boxy", ale můžeme volat metody a využívat invokedynamické a související funkce.

Podívejme se na třídu založenou na ASM pro vytvoření "Hello World" pomocí invokedynamic.

```java
public class InvokeDynamicCreator {

    public static void main(final String[] args) throws Exception {
        final String outputClassName = "kathik/Dynamic";
        try (FileOutputStream fos
                = new FileOutputStream(new File("target/classes/" + outputClassName + ".class"))) {
            fos.write(dump(outputClassName, "bootstrap", "()V"));
        }
    }

    public static byte[] dump(String outputClassName, String bsmName, String targetMethodDescriptor)
            throws Exception {
        final ClassWriter cw = new ClassWriter(0);
        MethodVisitor mv;

        // Setup the basic metadata for the bootstrap class
        cw.visit(V1_7, ACC_PUBLIC + ACC_SUPER, outputClassName, null, "java/lang/Object", null);

        // Create a standard void constructor
        mv = cw.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
        mv.visitCode();
        mv.visitVarInsn(ALOAD, 0);
        mv.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V");
        mv.visitInsn(RETURN);
        mv.visitMaxs(1, 1);
        mv.visitEnd();

        // Create a standard main method
        mv = cw.visitMethod(ACC_PUBLIC + ACC_STATIC, "main", "([Ljava/lang/String;)V", null, null);
        mv.visitCode();
        MethodType mt = MethodType.methodType(CallSite.class, MethodHandles.Lookup.class, String.class,
                MethodType.class);
        Handle bootstrap = new Handle(Opcodes.H_INVOKESTATIC, "kathik/InvokeDynamicCreator", bsmName,
                mt.toMethodDescriptorString());
        mv.visitInvokeDynamicInsn("runDynamic", targetMethodDescriptor, bootstrap);
        mv.visitInsn(RETURN);
        mv.visitMaxs(0, 1);
        mv.visitEnd();

        cw.visitEnd();

        return cw.toByteArray();
    }

    private static void targetMethod() {
        System.out.println("Hello World!");
    }

    public static CallSite bootstrap(MethodHandles.Lookup caller, String name, MethodType type) throws NoSuchMethodException, IllegalAccessException {
        final MethodHandles.Lookup lookup = MethodHandles.lookup();
        // Need to use lookupClass() as this method is static
        final Class<?> currentClass = lookup.lookupClass();
        final MethodType targetSignature = MethodType.methodType(void.class);
        final MethodHandle targetMH = lookup.findStatic(currentClass, "targetMethod", targetSignature);
        return new ConstantCallSite(targetMH.asType(type));
    }
}
```

Kód je rozdělen na dvě části, z nichž první používá rozhraní ASM Visitor API k vytvoření souboru třídy nazvaného kathik.Dynamic. Poznamenejte si klíčovou výzvu k návštěvěInvokeDynamicInsn (). Druhá část obsahuje cílovou metodu, která bude přivázána do místa volání, a BSM, který invokedynamický pokyn potřebuje.

Všimněte si, že tyto metody jsou v rámci třídy InvokeDynamicCreator a nejsou součástí naší generované třídy kathik.Dynamic. To znamená, že při běhu musí být InvokeDynamicCreator také na classpath a kathik.Dynamic, nebo metoda nebude možné najít.

Při spuštění aplikace InvokeDynamicCreator vytvoří nový soubor třídy Dynamic.class, který obsahuje invokedynamickou instrukci, jak vidíme pomocí javap na třídě:

```java
public static void main(java.lang.String[]);
    descriptor: ([Ljava/lang/String;)V
    flags: ACC_PUBLIC, ACC_STATIC
    Code:
      stack=0, locals=1, args_size=1
         0: invokedynamic #20,  0             // InvokeDynamic #0:runDynamic:()V
         5: return
```

Tento příklad ukázal nejjednodušší případ invokedynamic, který používá zvláštní případ konstantního objektu CallSite. To znamená, že BSM (a vyhledávání) se provádí pouze jednou a následné hovory jsou rychlé.

Nicméně, sofistikovanější využití invokedynamic může být komplexní rychle, obzvláště když volání místa a cílová metoda může měnit se během života programu.

V následujícím článku se budeme zabývat některými pokročilejšími případy použití a vytvoříme některé příklady, stejně jako budeme hlouběji zkoumat detaily invokedynamic.
