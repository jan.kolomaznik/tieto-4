package jm.java.generics.lesson;

import java.lang.reflect.Type;

public class GenericType<T> {

    private T type;

    public GenericType() {

    }

    public Class getType() {
        return type.getClass();
    }

    public static void main(String[] args) {
        GenericType<Integer> gt = new GenericType<>();
        System.out.println(gt.getType());
    }




}
