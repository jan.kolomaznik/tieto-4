package jm.java.generics.lesson;

public class Retezce {


    public static String simple(int size) {
        String result = "";
        for (int i = 0; i < size; i++) {
            result += Integer.toString(i);
            //result += " ";
        }
        return result;
    }

    public static String simple2(int size) {
        StringBuffer result = new StringBuffer();
        for (int i = 0; i < size; i++) {
            result.append(i);
            //result.append(" ");
        }
        return result.toString();
    }

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        simple(10_000);
        System.out.println(System.currentTimeMillis() - start);

        start = System.currentTimeMillis();
        simple2(100_000);
        System.out.println(System.currentTimeMillis() - start);

    }
}
