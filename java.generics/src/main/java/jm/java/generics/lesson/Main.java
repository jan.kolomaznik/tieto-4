package jm.java.generics.lesson;

import java.util.*;

public class Main implements Iterable<String> {

    public static void main(String[] args) {
        //List<Object> lo = new ArrayList<String>();

        List<Integer> seznam = Arrays.asList(1, 2, 3);
        System.out.println(suma(seznam));
        Integer i = 1;
        Number n = i;
        /*
        List<Integer> li = Arrays.asList(1, 2, 3);
        List<String>  ls = Arrays.asList("one", null, "two", "three");
        printList(li);
        printList(ls);
        */
    }

    public static void printList(List<?> list) {
        for (Object elem: list) {
            System.out.print(new StringBuilder().append(elem).append(" ").toString());
        }
    }

    public static double suma(Collection<? extends Number> cisla) {
        double result = 0;
        for (Number e : cisla) {
            result += e.doubleValue();
        }
        return result;
    }

    @Override
    public Iterator<String> iterator() {
        List<String> list = Arrays.asList("1", "2", "3");
        return list.iterator();

    }
}
