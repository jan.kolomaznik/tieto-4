package jm.java.generics.exercise;

public class Exercise {


    public static void main(String[] args) {
        // PUT api/nasedni/{carId = 1}/{zakaznikId = 2}
        Id<Auto> cadId = new Id<>(1);
        Id<Zakaznik> zakaznikId = new Id<>(2);

        String msg = nasedni(cadId, zakaznikId);
        System.out.println(msg);

    }

    public static String nasedni(Id<Auto> carId, Id<Zakaznik> zakaznikId) {
        Zakaznik z = Zakaznik.getZakaznik(zakaznikId);
        Auto a = Auto.getAuto(carId);
        return String.format("Zakaznik %s nasedl do auta %s.", z.getName(), a.getName());
    }
}
