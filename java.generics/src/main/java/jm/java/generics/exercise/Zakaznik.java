package jm.java.generics.exercise;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Zakaznik implements Comparable<Zakaznik> {

    private static Map<Long, Zakaznik> repository;

    public static Zakaznik getZakaznik(Id<Zakaznik> id) {
        if (repository == null) {
            repository = new HashMap<>();
            repository.put(1l, new Zakaznik(1, "Pepa"));
            repository.put(2l, new Zakaznik(2, "Jana"));
        }
        return repository.get(id.getValue());
    }

    private long id;

    private String name;

    private String surname;

    private int year;

    public Zakaznik(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Auto{" +
                "id=" + id +
                ", name=" + name +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Zakaznik zakaznik = (Zakaznik) o;
        return Objects.equals(name, zakaznik.name) &&
                Objects.equals(surname, zakaznik.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname);
    }

    @Override
    public int compareTo(Zakaznik o) {
        if (!Objects.equals(surname, o.surname)) {
            return surname.compareTo(o.surname);
        }

        if (!Objects.equals(name, o.name)) {
            return name.compareTo(o.name);
        }
        return year - o.year;
    }
}
