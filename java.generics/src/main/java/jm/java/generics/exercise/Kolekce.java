package jm.java.generics.exercise;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Kolekce {

    public static void main(String[] args) {

        List<String> list = new ArrayList<>();
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");

        System.out.println(Arrays.asList(1,2,3).getClass());
        System.out.println(list.getClass());
        System.out.println(Collections.unmodifiableList(list).getClass());
        System.out.println(Collections.synchronizedList(Collections.unmodifiableList(list)).getClass());

    }
}
