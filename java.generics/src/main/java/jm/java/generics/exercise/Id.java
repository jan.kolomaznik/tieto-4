package jm.java.generics.exercise;

/**
 * Generic ID class.
 * @param <T> Entity type
 */
public class Id<T> {

    private final long value;

    /**
     * Constructor with id value.
     * @param value of id in db.
     */
    public Id(long value) {
        this.value = value;
    }

    public long getValue() {
        return value;
    }
}
