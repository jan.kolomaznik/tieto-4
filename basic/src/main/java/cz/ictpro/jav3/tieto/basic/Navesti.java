package cz.ictpro.jav3.tieto.basic;

public class Navesti {

    public static void main(String[] args) {
        first: for (int i = 0; i < 10; i++) {
            second: for (int j = 0; j < 10; j++) {
                continue first;
            }
        }
    }
}
