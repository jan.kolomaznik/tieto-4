package cz.ictpro.jav3.tieto.basic;


import java.io.Serializable;
import java.util.Objects;

public abstract class HelloWorld extends Object implements Serializable {

    private static class Foo {

        public void boo() {
            System.out.println(greeting1);
        }
    }

    private static String greeting1 = "Hello world";
    private static final  String greeting2 = "Hello world";

    // Celočícelne
    volatile byte b = Byte.valueOf("12");
    transient short s = 0x5;
    int i = Integer.MAX_VALUE;
    long l = 10_000_000L;

    // Desetine
    float f = 10.0f;
    static double d = Double.POSITIVE_INFINITY;


    // ostatni
    boolean o = Boolean.TRUE;
    char c = 'c';
    Void v;

    public static void main(String[] args) throws Exception {
        System.out.println(Integer.MAX_VALUE + 1);

        System.out.println(greeting1 == greeting2);

        Integer a = null;
        Integer b = 128;

        System.out.println(Objects.equals(a, b));
    }

    public abstract HelloWorld foo();
}
