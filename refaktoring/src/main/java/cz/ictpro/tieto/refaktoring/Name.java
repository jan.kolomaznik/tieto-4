package cz.ictpro.tieto.refaktoring;

public class Name {

    private String value;

    public static Name name(String name) {
        return new Name(name);
    }

    private Name(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
