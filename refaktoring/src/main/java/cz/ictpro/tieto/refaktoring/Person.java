package cz.ictpro.tieto.refaktoring;

import static cz.ictpro.tieto.refaktoring.Name.name;
import static cz.ictpro.tieto.refaktoring.Age.*;

public class Person {

    private Name name;

    private Age age;

    public Person(Name name, Age age) {
        this.name = name;
        this.age = age;
    }

    public static void main(String[] args) {
        new Person(name("Pepa"), age(15));
        new Person(name("Pepa"), birthYear(2018));
    }
}
