package jm.java.streams.lesson;

import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.concurrent.Callable;

/**
 * Examples of using lambda in tricky situations.
 */
public final class Lambda_Traps {

    /**
     * Runs the program.
     *
     * @param args
     *            command line arguments. It must not be {@code null}.
     */
    public static void main(String... args) {
        final String user;
        // Sometimes the compiler can't infer the type: following is ambiguous
        // user = AccessController.doPrivileged(() -> System.getProperty("user.name"));
        // Casting to the proper interface type is necessary in such cases
        user = AccessController.doPrivileged((PrivilegedAction<String>) () -> System.getProperty("user.name"));
        System.out.println(user);

        // Sometimes resolution of the overloads might be difficult
        final String which = hello(() -> "Hello Lambda");
        System.out.println(which); // The return type of the lambda decides then!

        // Following fails - curiously, 'this' keyword in lambda expression
        // refers to the enclosing context, not to the lambda object itself
        // final Runnable r = () -> System.out.println(this); // FIXME

        // Following fails - lambda parameter names must not hide any enclosing
        // scope's local variable (as there is no way to refer to them otherwise)
        // final Function<String, String> hello = user -> "Hello " + user; // FIXME
    }

    static String hello(Callable<String> c) {
        return String.format("Hello from Callable (%s)", c);
    }

    static String hello(Runnable r) {
        return String.format("Hello from Runnable (%s)", r);
    }
}
