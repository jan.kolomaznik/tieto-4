package jm.java.streams.lesson.kalkulacka;

@FunctionalInterface
public interface Operace {

    /**
     * Vykonání operace, jedina metoda v interface!
     */
    int execute(int a, int b);

    default boolean isComutative(int a, int b) {
        return execute(a, b) == execute(b, a);
    }
}
