package jm.java.streams.lesson;

import java.util.Date;
import java.util.function.Function;
import java.util.function.Supplier;

public class DateGenerator {

    public static void main(String[] args) {

        Supplier<Date> bezParam = Date::new;
        Function<Long, Date> sParam = Date::new;

        System.out.println(bezParam.get());
        System.out.println(sParam.apply(0L));

    }
}
