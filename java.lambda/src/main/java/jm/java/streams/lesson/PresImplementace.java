package jm.java.streams.lesson;

import java.util.function.ToIntFunction;

public class PresImplementace {

    public interface A {

        int a(String s);
    }

    public interface B {

        int b(String s);
    }

    public interface C {

        int c(String s);
    }

    public static class AB implements A,B {

        @Override
        public int a(String s) {
            return 1;
        }

        @Override
        public int b(String s) {
            return 2;
        }
    }

    public static void main(String[] args) {
        AB ab = new AB();

        A aFoo = ab;
        B bFoo = ab;

        //C cFoo = ab::a;
        C cFoo = new C() {
            @Override
            public int c(String s) {
                return 0;
            }
        };

        System.out.println(aFoo.a("x"));
        System.out.println(bFoo.b("x"));

    }
}
