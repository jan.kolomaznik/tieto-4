package jm.java.streams.lesson.kalkulacka;

import java.util.Deque;
import java.util.LinkedList;

public class Main {

    public static void main(String[] args) {
        Operace soucet = (a, b) -> a + b;
        Operace rozdil = (a, b) -> a - b;
        Operace nasobeni = (a, b) -> a * b;
        Operace deleni = (a, b) -> a / b;

        Deque<Operace> batch = new LinkedList<>();
        batch.addLast((a, b) -> a + b);
        batch.addLast(rozdil);
        batch.addLast(nasobeni);
        batch.addLast(Main::distance);

        execute(batch, 1,2);
        execute(batch, 8, 9);


    }

    public static int distance(int a, int b) {
        return Math.abs(a - b);
    }

    public static void execute(Deque<Operace> batch, int a, int b) {
        for (Operace operace: batch) {
            System.out.printf("%d, %d -> %d; (%s)%n", a, b,
                    operace.execute(a, b),
                    operace.isComutative(a, b));
        }
    }
}
