package jm.java.streams.lesson;

import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.Callable;

/**
 * Examples of using lambda syntax.
 */
public final class Lambda_Syntax {

    /**
     * A home-made version of {@link java.util.function.ToIntFunction} (assuming
     * this was available earlier).
     *
     * <p>
     * There is only one method required to be implemented, such an interface is
     * called <em>functional</em> and can be used for lamda expressions.
     *
     * @param <T>
     *            the type of argument
     */

    private interface ToIntFunction<T> {

        int apply(T value);
    }


    /**
     * Prints transformed values.
     *
     * @param values
     *            the values to transform and print; it must not be {@code null}
     * @param f
     *            the transformation to apply; it must not be {@code null}
     */
    private static <T> void printResults(Iterable<? extends T> values, ToIntFunction<T> f) {
        for (T value : values) {
            System.out.format("%s -> %d%n", value, f.apply(value));
        }
    }

    /**
     * Runs the program.
     *
     * @param args
     *            command line arguments. It must not be {@code null}.
     *
     * @throws Exception
     *             if something goes wrong
     */
    public static void main(String... args) throws Exception {
        final Collection<String> values = Arrays.asList("one", "two", "three", "");

        // Traditional usage before Java 8: no lambda
        final ToIntFunction<String> length = new ToIntFunction<String>() {

            public int apply(String value) {
                return value.length();
            }
        };

        printResults(values, length);

        // Traditional usage before Java 8: no lambda, just more compact form
        printResults(values, new ToIntFunction<String>() {

            public int apply(String value) {
                return value.length();
            }
        });

        // Java 8: explicitly defined lambda in various ways
        final ToIntFunction<String> lambda1 = (final String s) -> {
            return s.length();
        };
        printResults(values, lambda1);
        final ToIntFunction<String> lambda2 = (String s) -> s.length();
        printResults(values, lambda2);
        final ToIntFunction<String> lambda3 = s -> s.length();
        printResults(values, lambda3);
        final ToIntFunction<String> lambda4 = String::length;
        printResults(values, lambda4);
        final ToIntFunction<Object> lambda5 = o -> 42;
        printResults(values, lambda5);

        // For completeness of the syntax demonstration
        final Callable<Integer> callable = () -> 42;
        System.out.println(callable.call());

        // Java8: implicitly defined lambda as above
        printResults(values, (final String s) -> {
            return s.length();
        });
        printResults(values, (String s) -> s.length());
        printResults(values, s -> s.length());
        printResults(values, Lambda_Syntax::aIndex);
        printResults(values, o -> 42);
    }

    public static class MyToIntFuction implements ToIntFunction {

        @Override
        public int apply(Object value) {
            return 0;
        }
    }

    public static int aIndex(String s) {
        return s.indexOf("a");
    }
}
