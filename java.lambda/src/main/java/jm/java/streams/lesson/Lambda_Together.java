package jm.java.streams.lesson;

import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * Examples of virtual extension methods.
 */
public final class Lambda_Together {

    /**
     * An extension of a predicate to be applicable as a condition too.
     *
     * @param <T>
     *            the type of the parameter
     */
    public interface Condition<T> extends Predicate<T> {

        /**
         * Returns a conditional {@link Consumer} instance.
         *
         * @param consumer
         *            the consumer to decorate; it must not be {@code null}
         *
         * @return a decorated consumer
         */
        default Consumer<T> applyOn(Consumer<? super T> consumer) {
            assert consumer != null: "Consumer is null";

            //Objects.requireNonNull(consumer);
            return value -> {
                if (test(value)) {
                    consumer.accept(value);
                }
            };
        }
    }

    /**
     * Runs the program.
     *
     * @param args
     *            command line arguments. It must not be {@code null}.
     */
    public static void main(String... args) {
        final Condition<String> condition = t -> (t != null) && !t.isEmpty();

        condition.applyOn(null);

        final Consumer<String> out = condition.applyOn(System.out::println);
        out.accept("first");
        out.accept(null);
        out.accept("");
        out.accept("fourth");

        // An example of direct construction, cast necessary due to type ambiguity
        final Consumer<String> err = ((Condition<String>) (String t) -> !t.isEmpty()).applyOn(System.err::println);
        err.accept("first");
        err.accept("");
        err.accept("third");
    }
}
