package cz.ictpro.tieto.usage;

import cz.ictpro.tieto.validator.NotNull;

public class Person {

    @NotNull(critical = true)
    private String name;

    @NotNull
    private String suname;

    private int age;

    public Person(String  name, String suname, int age) {
        this.name = name;
        this.suname = suname;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", suname='" + suname + '\'' +
                ", age=" + age +
                '}';
    }
}
