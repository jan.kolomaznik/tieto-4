package cz.ictpro.tieto.usage;

import cz.ictpro.tieto.validator.Validator;

public class Main {

    public static void main(String[] args) {
        Validator validator = new Validator();

        Person honza = new Person("Jan", "Kolomaznik", 30);
        Person jirka = new Person("Jirka", null, 40);
        Person pirat = new Person(null, "Sparrow", 50);

        validator.notNullCheck(honza);
        validator.notNullCheck(jirka);
        validator.notNullCheck(pirat);

    }
}
