package cz.ictpro.tieto.validator;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

public class Validator {

    public void notNullCheck(Object o) {
        Class oClass = o.getClass();

        System.out.println(o);
        for (Field field: oClass.getDeclaredFields()) {
            try {
                Annotation annotation = field.getAnnotation(NotNull.class);
                if (annotation == null) {
                    continue;
                }

                field.setAccessible(true);
                NotNull notNull = (NotNull) annotation;
                Object value = field.get(o);

                if (value == null) {
                    String msg = String.format("Field %s is null!", field.getName());
                    if (notNull.critical()) {
                        throw new IllegalStateException(msg);
                    } else {
                        System.err.println(msg);
                    }
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        System.out.println("---");
    }
}
